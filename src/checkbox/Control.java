package checkbox;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Control {
	private View view ;
	private ActionListener actionListener1 ;
	private ActionListener actionListener2 ;
	private ActionListener actionListener3 ;
	
	public Control(View view){
		this.view = view;
	}
	
	public void acListener()
	{
		actionListener1 = new ActionListener(){
			public void actionPerformed(ActionEvent actionEvent) 
			{ 
				if(view.getRCheckBox().isSelected() && view.getGCheckBox().isSelected() && view.getBCheckBox().isSelected()) 
					view.getColor().setBackground(Color.WHITE);
				else if(view.getRCheckBox().isSelected() && view.getGCheckBox().isSelected()) 
					view.getColor().setBackground(Color.YELLOW);
				else if(view.getRCheckBox().isSelected() && view.getBCheckBox().isSelected()) 
					view.getColor().setBackground(Color.PINK);
				else if(view.getGCheckBox().isSelected() && view.getBCheckBox().isSelected()) 
					view.getColor().setBackground(Color.CYAN);
				else if(view.getRCheckBox().isSelected()) 
					view.getColor().setBackground(Color.RED);
				else if(view.getGCheckBox().isSelected()) 
					view.getColor().setBackground(Color.GREEN);
				else if(view.getBCheckBox().isSelected()) 
					view.getColor().setBackground(Color.BLUE);
				else
					view.getColor().setBackground(null);
				
			}
		};
		
		view.getRCheckBox().addActionListener(actionListener1); 
		view.getGCheckBox().addActionListener(actionListener1); 
		view.getBCheckBox().addActionListener(actionListener1); 
	}
	
	public static void main(String[] args) {
		View v = new View();
		Control c = new Control(v);
		c.acListener();

	}

}
