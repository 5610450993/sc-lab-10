package checkbox;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class View extends JFrame{
	private JPanel controlPanel, colorPanel, buttonPanel ;
	private JCheckBox redCheckBox, greenCheckBox, blueCheckBox ;
	
	public View(){
		createFrame();
	}
	
	public void createFrame(){
		setTitle("Checkbox_Color");
		setSize(300, 400);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		colorPanel = new JPanel();
		
		buttonPanel = new JPanel();
		redCheckBox = new JCheckBox("Red");
		greenCheckBox = new JCheckBox("Green");
		blueCheckBox = new JCheckBox("Blue");
		
		buttonPanel.add(redCheckBox);
		buttonPanel.add(greenCheckBox);
		buttonPanel.add(blueCheckBox);
		
		controlPanel = new JPanel(new BorderLayout());
		controlPanel.add(new JPanel(),BorderLayout.NORTH);
		controlPanel.add(new JPanel(),BorderLayout.EAST);
		controlPanel.add(new JPanel(),BorderLayout.WEST);
		controlPanel.add(colorPanel,BorderLayout.CENTER);
		controlPanel.add(buttonPanel,BorderLayout.SOUTH);
		
		add(controlPanel);
		
		setVisible(true);
		setResizable(false);
	}
	
	public JCheckBox getRCheckBox(){
		return redCheckBox;
	}
	
	public JCheckBox getGCheckBox(){
		return greenCheckBox;
	}
	
	public JCheckBox getBCheckBox(){
		return blueCheckBox;
	}
	
	public JPanel getColor(){
		return colorPanel;
	}
}
