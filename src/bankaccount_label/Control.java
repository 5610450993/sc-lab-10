package bankaccount_label;



import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Control {
	private Model model ;
	private View view ;
	private ActionListener actionListener1 ;
	private ActionListener actionListener2 ;
	
	public Control(Model model, View view){
		this.model = model;
		this.view = view;
	}
	
	public void acListener()
	{
		actionListener1 = new ActionListener(){
			public void actionPerformed(ActionEvent actionEvent) 
			{ 
				double amount = Double.parseDouble(view.getManage().getText());
				model.deposit(amount);
				view.showBalance().setText("Balance: "+model.getBalance()+" ");
				view.getManage().setText("");
			}
		};
		
		actionListener2 = new ActionListener(){
			public void actionPerformed(ActionEvent actionEvent) 
			{ 
				double amount = Double.parseDouble(view.getManage().getText());
				model.withdraw(amount);
				view.showBalance().setText("Balance: "+model.getBalance()+" ");
				view.getManage().setText("");
			}
		};
		
		view.getDepositButton().addActionListener(actionListener1); 
		view.getWithdrawButton().addActionListener(actionListener2); 
	}
	
	public static void main(String[] args) {
		Model m = new Model();
		View v = new View();
		Control c = new Control(m,v);
		c.acListener();

	}

}
