package bankaccount_label;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class View extends JFrame{
	private JPanel controlPanel, managePanel, buttonPanel, gridPanel ;
	private JButton deposit, withdraw ;
	private JLabel amount, balance ;
	private JTextField manage ;
	
	public View(){
		createFrame();
	}
	
	public void createFrame(){
		setTitle("Button_Color");
		setSize(500, 150);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		managePanel = new JPanel();
		amount = new JLabel("Amount: ");
		manage = new JTextField(13);
		balance = new JLabel("Balance: 0.0 ");
		managePanel.add(amount);
		managePanel.add(manage);
		managePanel.add(balance);
		
		buttonPanel = new JPanel();
		deposit = new JButton("Deposit");
		withdraw = new JButton("Withdraw");
		buttonPanel.add(deposit);
		buttonPanel.add(withdraw);
		
		gridPanel = new JPanel(new GridLayout(2,1));
		gridPanel.add(managePanel);
		gridPanel.add(buttonPanel);
		
		controlPanel = new JPanel(new BorderLayout());
		controlPanel.add(new JPanel(),BorderLayout.NORTH);
		controlPanel.add(gridPanel,BorderLayout.CENTER);
		
		add(controlPanel);
		
		setVisible(true);
		setResizable(false);
	}
	
	public JButton getDepositButton(){
		return deposit;
	}
	
	public JButton getWithdrawButton(){
		return withdraw;
	}
	
	public JTextField getManage(){
		return manage;
	}
	
	public JLabel showBalance(){
		return balance;
	}
}
