package button;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class View extends JFrame{
	private JPanel controlPanel, colorPanel, buttonPanel ;
	private JButton redButton, greenButton, blueButton ;
	
	public View(){
		createFrame();
	}
	
	public void createFrame(){
		setTitle("Button_Color");
		setSize(300, 400);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		colorPanel = new JPanel();
		
		buttonPanel = new JPanel();
		redButton = new JButton("Red");
		greenButton = new JButton("Green");
		blueButton = new JButton("Blue");
		
		buttonPanel.add(redButton);
		buttonPanel.add(greenButton);
		buttonPanel.add(blueButton);
		
		controlPanel = new JPanel(new BorderLayout());
		controlPanel.add(new JPanel(),BorderLayout.NORTH);
		controlPanel.add(new JPanel(),BorderLayout.EAST);
		controlPanel.add(new JPanel(),BorderLayout.WEST);
		controlPanel.add(colorPanel,BorderLayout.CENTER);
		controlPanel.add(buttonPanel,BorderLayout.SOUTH);
		
		add(controlPanel);
		
		setVisible(true);
		setResizable(false);
	}
	
	public JButton getRButton(){
		return redButton;
	}
	
	public JButton getGButton(){
		return greenButton;
	}
	
	public JButton getBButton(){
		return blueButton;
	}
	
	public JPanel getColor(){
		return colorPanel;
	}
}
