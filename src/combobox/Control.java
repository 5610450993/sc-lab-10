package combobox;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Control {
	private View view ;
	private ActionListener actionListener1 ;
	
	public Control(View view){
		this.view = view;
	}
	
	public void acListener()
	{
		actionListener1 = new ActionListener(){
			public void actionPerformed(ActionEvent actionEvent) 
			{ 
				if(view.getColorCombo().getSelectedItem().equals("Red")) view.getColor().setBackground(Color.RED);
				else if(view.getColorCombo().getSelectedItem().equals("Green")) view.getColor().setBackground(Color.GREEN);
				else if(view.getColorCombo().getSelectedItem().equals("Blue")) view.getColor().setBackground(Color.BLUE);
			}
		};
			
		view.getColorCombo().addActionListener(actionListener1); 

	}
	
	public static void main(String[] args) {
		View v = new View();
		Control c = new Control(v);
		c.acListener();

	}

}
