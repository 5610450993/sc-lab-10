package combobox;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class View extends JFrame{
	private JPanel controlPanel, colorPanel, buttonPanel ;
	private JComboBox<String> colorCombo ;
	private String[] menu = {"Red","Green","Blue"};
	private ButtonGroup radioGroup;
	
	public View(){
		createFrame();
	}
	
	public void createFrame(){
		setTitle("Combobox_Color");
		setSize(300, 400);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		colorPanel = new JPanel();
		colorPanel.setBackground(Color.RED);
		
		buttonPanel = new JPanel();
		colorCombo = new JComboBox<String>(menu);
		
		buttonPanel.add(colorCombo);
		
		controlPanel = new JPanel(new BorderLayout());
		controlPanel.add(new JPanel(),BorderLayout.NORTH);
		controlPanel.add(new JPanel(),BorderLayout.EAST);
		controlPanel.add(new JPanel(),BorderLayout.WEST);
		controlPanel.add(colorPanel,BorderLayout.CENTER);
		controlPanel.add(buttonPanel,BorderLayout.SOUTH);
		
		add(controlPanel);
		
		setVisible(true);
		setResizable(false);
	}
	
	public JComboBox<String> getColorCombo(){
		return colorCombo;
	}
	
	public JPanel getColor(){
		return colorPanel;
	}
}
