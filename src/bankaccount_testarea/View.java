package bankaccount_testarea;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.ScrollPane;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class View extends JFrame{
	private JPanel controlPanel, managePanel, buttonPanel, areaPanel ;
	private JButton deposit, withdraw ;
	private JTextArea balance ;
	private JTextField manage ;
	private JLabel amount ;
	private ScrollPane scroll;
	private String info = "Balance: 0.0\n\n";
	
	public View(){
		createFrame();
	}
	
	public void createFrame(){
		setTitle("Button_Color");
		setSize(500, 220);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		managePanel = new JPanel();
		amount = new JLabel("Amount: ");
		manage = new JTextField(13);
		managePanel.add(amount);
		managePanel.add(manage);
		
		buttonPanel = new JPanel();
		deposit = new JButton("Deposit");
		withdraw = new JButton("Withdraw");
		buttonPanel.add(deposit);
		buttonPanel.add(withdraw);
		
		areaPanel = new JPanel();
		scroll = new ScrollPane();
		scroll.setSize(350, 100);
		balance = new JTextArea(info);
		scroll.add(balance);
		areaPanel.add(scroll);
		
		controlPanel = new JPanel(new BorderLayout());
		controlPanel.add(areaPanel,BorderLayout.NORTH);
		controlPanel.add(managePanel,BorderLayout.CENTER);
		controlPanel.add(buttonPanel,BorderLayout.SOUTH);
		
		add(controlPanel);
		
		setVisible(true);
		setResizable(false);
	}
	
	public JButton getDepositButton(){
		return deposit;
	}
	
	public JButton getWithdrawButton(){
		return withdraw;
	}
	
	public JTextField getManage(){
		return manage;
	}
	
	public JTextArea showBalance(){
		return balance;
	}
	
	public String getInfo(String info){
		this.info += info+"\n\n";
		return this.info;
	}
}
