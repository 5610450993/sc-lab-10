package radiobutton;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class View extends JFrame{
	private JPanel controlPanel, colorPanel, buttonPanel ;
	private JRadioButton redRadio, greenRadio, blueRadio ;
	private ButtonGroup radioGroup;
	
	public View(){
		createFrame();
	}
	
	public void createFrame(){
		setTitle("Radio_Button_Color");
		setSize(300, 400);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		colorPanel = new JPanel();
		
		buttonPanel = new JPanel();
		redRadio = new JRadioButton("Red");
		greenRadio = new JRadioButton("Green");
		blueRadio = new JRadioButton("Blue");
		
		radioGroup = new ButtonGroup();
		radioGroup.add(redRadio);
		radioGroup.add(greenRadio);
		radioGroup.add(blueRadio);
		
		buttonPanel.add(redRadio);
		buttonPanel.add(greenRadio);
		buttonPanel.add(blueRadio);
		
		controlPanel = new JPanel(new BorderLayout());
		controlPanel.add(new JPanel(),BorderLayout.NORTH);
		controlPanel.add(new JPanel(),BorderLayout.EAST);
		controlPanel.add(new JPanel(),BorderLayout.WEST);
		controlPanel.add(colorPanel,BorderLayout.CENTER);
		controlPanel.add(buttonPanel,BorderLayout.SOUTH);
		
		add(controlPanel);
		
		setVisible(true);
		setResizable(false);
	}
	
	public JRadioButton getRRadio(){
		return redRadio;
	}
	
	public JRadioButton getGRadio(){
		return greenRadio;
	}
	
	public JRadioButton getBRadio(){
		return blueRadio;
	}
	
	public JPanel getColor(){
		return colorPanel;
	}
}
