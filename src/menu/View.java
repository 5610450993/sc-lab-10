package menu;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class View extends JFrame{
	private JPanel controlPanel, colorPanel, buttonPanel ;
	private JMenuItem redItem, greenItem, blueItem ;
	private JMenuBar menuBar ;
	private JMenu menuColor ;
	
	public View(){
		createFrame();
	}
	
	public void createFrame(){
		setTitle("Button_Color");
		setSize(300, 400);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		colorPanel = new JPanel();
		
		menuColor = new JMenu("Color");
		redItem = new JMenuItem("Red");
		greenItem = new JMenuItem("Green");
		blueItem = new JMenuItem("Blue");
		
		menuColor.add(redItem);
		menuColor.add(greenItem);
		menuColor.add(blueItem);
		
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		menuBar.add(menuColor);
		
		
		controlPanel = new JPanel(new BorderLayout());
		controlPanel.add(new JPanel(),BorderLayout.NORTH);
		controlPanel.add(new JPanel(),BorderLayout.EAST);
		controlPanel.add(new JPanel(),BorderLayout.WEST);
		controlPanel.add(new JPanel(),BorderLayout.SOUTH);
		controlPanel.add(colorPanel,BorderLayout.CENTER);
		
		add(controlPanel);
		
		setVisible(true);
		setResizable(false);
	}
	
	public JMenuItem getRItem(){
		return redItem;
	}
	
	public JMenuItem getGItem(){
		return greenItem;
	}
	
	public JMenuItem getBItem(){
		return blueItem;
	}
	
	public JPanel getColor(){
		return colorPanel;
	}
}
