package menu;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Control {
	private View view ;
	private ActionListener actionListener1 ;
	private ActionListener actionListener2 ;
	private ActionListener actionListener3 ;
	
	public Control(View view){
		this.view = view;
	}
	
	public void acListener()
	{
		actionListener1 = new ActionListener(){
			public void actionPerformed(ActionEvent actionEvent) 
			{ 
				view.getColor().setBackground(Color.RED);
			}
		};
		
		actionListener2 = new ActionListener(){
			public void actionPerformed(ActionEvent actionEvent) 
			{ 
				view.getColor().setBackground(Color.GREEN);
			}
		};
		
		
		actionListener3 = new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) 
			{ 
				view.getColor().setBackground(Color.BLUE);
			}
		};
		
		view.getRItem().addActionListener(actionListener1); 
		view.getGItem().addActionListener(actionListener2); 
		view.getBItem().addActionListener(actionListener3); 
	}
	
	public static void main(String[] args) {
		View v = new View();
		Control c = new Control(v);
		c.acListener();

	}

}
